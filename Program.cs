﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05._12task1
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b, c, d;
            Console.Write("Введите первую оценку: ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Введите вторую оценку: ");
            b = int.Parse(Console.ReadLine());
            Console.Write("Введите третью оценку: ");
            c = int.Parse(Console.ReadLine());
            Console.Write("Введите четвертую оценку: ");
            d = int.Parse(Console.ReadLine());

            int sum;
            sum = a + b + c + d;

            Console.WriteLine($"Сумма баллов: {sum}");
            Console.ReadKey();
        
        }
    }
}
